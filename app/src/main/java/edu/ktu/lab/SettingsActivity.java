package edu.ktu.lab;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.SeekBar;

public class SettingsActivity extends AppCompatActivity {

    public static final String MY_PREFERENCES = "GamePreferences";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        loadSettings();
    }

    void loadSettings()
    {
        SharedPreferences sharedPrefs = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE);
        boolean soundEnabled = sharedPrefs.getBoolean("soundEnabled", true);
        String difficulty = sharedPrefs.getString("difficulty", "medium");
        String playerName = sharedPrefs.getString("playerName", "Player");
        float volume = sharedPrefs.getFloat("soundVolume", 1f);

        CheckBox soundEnabledBox = (CheckBox)findViewById(R.id.sound_box);
        RadioGroup difficultyGroup = (RadioGroup)findViewById(R.id.difficulty_group);
        EditText playerNameEdit = (EditText)findViewById(R.id.player_name_field);
        SeekBar volumeBar = (SeekBar)findViewById(R.id.seekBar);

        soundEnabledBox.setChecked(soundEnabled);
        switch (difficulty)
        {
            case "easy":
                difficultyGroup.check(R.id.radio_easy);
                break;
            case "medium":
                difficultyGroup.check(R.id.radio_medium);
                break;
            case "hard":
                difficultyGroup.check(R.id.radio_hard);
                break;
        }

        volumeBar.setProgress((int)(volumeBar.getMax() * volume));

        playerNameEdit.setText(playerName);
    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.save_btn)
        {
            saveSettings();
        }
    }

    void saveSettings()
    {
        SharedPreferences.Editor sharedPrefEditor = getSharedPreferences(MY_PREFERENCES, Context.MODE_PRIVATE).edit();

        CheckBox soundEnabledBox = (CheckBox)findViewById(R.id.sound_box);
        RadioGroup difficultyGroup = (RadioGroup)findViewById(R.id.difficulty_group);
        EditText playerNameEdit = (EditText)findViewById(R.id.player_name_field);
        SeekBar volumeBar = (SeekBar)findViewById(R.id.seekBar);

        sharedPrefEditor.putBoolean("soundEnabled", soundEnabledBox.isChecked());
        String difficulty = "easy";
        switch(difficultyGroup.getCheckedRadioButtonId())
        {
            case R.id.radio_easy:
                difficulty = "easy";
                break;
            case R.id.radio_medium:
                difficulty = "medium";
                break;
            case R.id.radio_hard:
                difficulty = "hard";
                break;
        }
        sharedPrefEditor.putString("difficulty", difficulty);
        sharedPrefEditor.putString("playerName", playerNameEdit.getText().toString());

        sharedPrefEditor.putFloat("soundVolume", ((float)volumeBar.getProgress() / (float)volumeBar.getMax()));

        sharedPrefEditor.commit();
    }
}
