package edu.ktu.lab;

import android.graphics.Bitmap;

/**
 * Created by super on 10/6/2016.
 */
public class LeaderboardEntry {

    private int mID;
    private String mName;
    private int mScore;

    public LeaderboardEntry()
    {
        mID = 0;
        mName = "";
        mScore = 0;
    }

    public LeaderboardEntry(int id, String name, int score)
    {
        mID = id;
        mName = name;
        mScore = score;
    }

    public void setID(int val)
    {
        mID = val;
    }

    public int getID()
    {
        return mID;
    }

    public void setName(String val)
    {
        mName = val;
    }

    public String getName()
    {
        return mName;
    }

    public void setScore(int val)
    {
        mScore = val;
    }

    public int getScore()
    {
        return mScore;
    }
}
