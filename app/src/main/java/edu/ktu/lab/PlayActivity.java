package edu.ktu.lab;

import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import java.sql.Time;
import java.util.Random;

public class PlayActivity extends AppCompatActivity {

    private final static int GAME_END_PLAYER_WON = 0;
    private final static int GAME_END_COMPUTER_WON = 1;
    private final static int GAME_END_DRAW = 3;

    private int[] mCells = new int[] {0,0,0,
            0,0,0,
            0,0,0};

    private ImageView[] mCellViews;

    private int mPlayerSymbol = 1;
    private int mEnemySymbol = 2;

    Random mRandom;

    boolean mGameEnded = false;
    int mWinningPlayer = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        init();
        updateCells();
    }

    private void init()
    {
        mRandom = new Random(System.currentTimeMillis());
        initCells();
        initPlayers();
    }

    private void initCells()
    {
        mCellViews = new ImageView[9];
        mCellViews[0] = (ImageView)findViewById(R.id.cell0);
        mCellViews[1] = (ImageView)findViewById(R.id.cell1);
        mCellViews[2] = (ImageView)findViewById(R.id.cell2);
        mCellViews[3] = (ImageView)findViewById(R.id.cell3);
        mCellViews[4] = (ImageView)findViewById(R.id.cell4);
        mCellViews[5] = (ImageView)findViewById(R.id.cell5);
        mCellViews[6] = (ImageView)findViewById(R.id.cell6);
        mCellViews[7] = (ImageView)findViewById(R.id.cell7);
        mCellViews[8] = (ImageView)findViewById(R.id.cell8);
    }

    private void initPlayers()
    {
        mPlayerSymbol = (mRandom.nextInt(2) == 1) ? 2 : 1;
        mEnemySymbol = 3 - mPlayerSymbol;
    }

    private void updateCells()
    {
        for(int i = 0; i < mCells.length; i++)
        {
            switch(mCells[i])
            {
                case 0:
                    mCellViews[i].setAlpha(0f);
                    break;
                case 1:
                    mCellViews[i].setAlpha(1f);
                    mCellViews[i].setImageResource(R.drawable.cross);
                    break;
                case 2:
                    mCellViews[i].setAlpha(1f);
                    mCellViews[i].setImageResource(R.drawable.o);
                    break;
            }
        }
    }

    public void onClick(View v)
    {
        boolean playerMarked = false;
        for(int i = 0; i < mCellViews.length; i++)
        {
            if(v.getId() == mCellViews[i].getId())
            {
                markCell(i, mPlayerSymbol);
                break;
            }
        }
        processAI();
        updateCells();
    }

    private void markCell(int index, int symbol)
    {
        if(!mGameEnded) {
            if (mCells[index] == 0) {
                mCells[index] = symbol;
            }
            if (didAnyoneWin()) {
                if (mWinningPlayer == mPlayerSymbol) {
                    Log.d("GAME", "Player won!");
                } else {
                    Log.d("GAME", "Computer won!");
                }
                mGameEnded = true;
            }
            if (movesLeft() == 0 && !mGameEnded) {
                draw();
            }
        }
    }

    private void draw()
    {
        mGameEnded = true;
        Log.d("GAME", "It's a draw!");
    }

    private void processAI()
    {
        if(movesLeft() > 0) {
            Random rand = new Random(System.nanoTime());
            int desiredCell = rand.nextInt(9);
            while (mCells[desiredCell] != 0) {
                desiredCell = rand.nextInt(9);
            }
            markCell(desiredCell, mEnemySymbol);
        }
    }

    private boolean didAnyoneWin()
    {
        int cellValue = 0;
        boolean victory = false;
        int winningSymbol = -1;
        for(int i = 0; i < 3; i++)
        {
            cellValue = mCells[i*3];
            if(cellValue != 0 && mCells[i*3+1] == cellValue && mCells[i*3+2] == cellValue)
            {
                winningSymbol = cellValue;
                victory = true;
            }

        }
        for(int i = 0; i < 3; i++)
        {
            cellValue = mCells[i];
            if(cellValue != 0 && mCells[i+3] == cellValue && mCells[i+6] == cellValue)
            {
                winningSymbol = cellValue;
                victory = true;
            }
        }
        cellValue = mCells[0];
        if(cellValue != 0 && mCells[4] == cellValue && mCells[8] == cellValue)
        {
            winningSymbol = cellValue;
            victory = true;
        }
        cellValue = mCells[2];
        if(cellValue != 0 && mCells[4] == cellValue && mCells[6] == cellValue)
        {
            winningSymbol = cellValue;
            victory = true;
        }
        if(victory)
        {
            if(winningSymbol == mPlayerSymbol)
            {
                mWinningPlayer = 0;
            }
            else
            {
                mWinningPlayer = 1;
            }
        }
        return victory;
    }

    private int movesLeft()
    {
        int accum = 0;
        for(int i = 0; i < mCells.length; i++)
        {
            if(mCells[i] == 0)
            {
                accum++;
            }
        }
        return accum;
    }
}
