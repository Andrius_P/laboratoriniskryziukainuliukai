package edu.ktu.lab;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Permission;
import java.util.jar.Manifest;

public class MainActivity extends AppCompatActivity {

    private static final int INT_REQUEST_RESULT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onClick(View v)
    {
        if(v.getId() == R.id.play_btn)
        {
            Intent intent = new Intent(this, PlayActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.settings_btn)
        {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.profile_btn)
        {
            Intent intent = new Intent(this, ProfileActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.about_btn)
        {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }
        if(v.getId() == R.id.leaderboard_btn)
        {
            Intent intent = new Intent(this, LeaderboardActivity.class);
            startActivity(intent);
        }
    }

    class AsyncGet extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... params)
        {
            StringBuilder builder = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                String line;
                while((line = reader.readLine()) != null)
                {
                    builder.append(line);
                }
                Log.d("TAG", builder.toString());
                urlConnection.disconnect();
            }
            catch(Exception e)
            {

            }
            return builder.toString();
        }

        @Override
        protected void onPostExecute(String result)
        {
            if(result != null)
            {
                Log.d("TAG", result);
                string2Json(result);
            }
        }
    }

    public void string2Json(String string)
    {
        try {
            JSONObject object = new JSONObject(string);
            JSONArray array = object.getJSONArray("leaderboard");
            for(int i = 0; i < array.length(); i++)
            {
                Log.d("TAGGERINO", array.getJSONObject(i).toString());
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
